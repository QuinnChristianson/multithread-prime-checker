﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Homework3 {
    internal class Calculator {

        /*
         * I worked with Andrew Dyer on this homework
         */

        public void Run(NumberReader reader) {
            var results = new List<long>();
            var numbersToCheck = new Queue<long>();
            
            foreach (var value in reader.ReadIntegers())
            {
                numbersToCheck.Enqueue(value);
            }

            StartComputationThreads(results, numbersToCheck);

            var progressMonitor = new ProgressMonitor(results);

            Thread r = new Thread(progressMonitor.Run); 
            r.IsBackground = true;
            r.Start();//{IsBackground = true}.Start();

            while (numbersToCheck.Count > 0 || results.Count > 0)
            {
                Thread.Sleep(100);
            }

            r.Abort();
            Console.WriteLine("{0} of the numbers were prime", progressMonitor.TotalCount);
        }

        private static void StartComputationThreads(List<long> results, Queue<long> numbersToCheck) {
            var threads = CreateThreads(results, numbersToCheck);
            threads.ForEach(thread => thread.Start());
        }
        
        private static List<Thread> CreateThreads(List<long> results, Queue<long> numbersToCheck) {
            var threadCount = Environment.ProcessorCount * 2;

            Console.WriteLine("Using {0} compute threads and 1 I/O thread", threadCount);

            var threads =
                (from threadNumber in Sequence.Create(0, threadCount)
                    let calculator = new IsNumberPrimeCalculator(results, numbersToCheck)
                    let newThread =
                        new Thread(calculator.CheckIfNumbersArePrime) {
                            IsBackground = true,
                            Priority = ThreadPriority.BelowNormal
                        }
                    select newThread).ToList();
            return threads;
        }
    }
}