﻿
using System;
using System.Collections.Generic;
using System.Threading;


namespace Homework3 {

    internal class IsNumberPrimeCalculator {
        private readonly ICollection<long> _primeNumbers;
        private readonly Queue<long> _numbersToCheck;

        public IsNumberPrimeCalculator(ICollection<long> primeNumbers, Queue<long> numbersToCheck) {
            _primeNumbers = primeNumbers;
            _numbersToCheck = numbersToCheck;
        }
        public void CheckIfNumbersArePrime() {
            while (true) {
                long numberToCheck;

                lock (_numbersToCheck)
                {
                    if (_numbersToCheck.Count > 0)
                    {
                        numberToCheck = _numbersToCheck.Dequeue();
                    }
                    else
                    {
                        break;
                    }
                }

                if (IsNumberPrime(numberToCheck))
                {

                    lock (_primeNumbers)
                    {
                        _primeNumbers.Add(numberToCheck);
                    }

                }
               
                if (_numbersToCheck.Count == 0)
                {
                    break;
                }
            }
        }

        private bool IsNumberPrime(long numberWeAreChecking) {
            const long firstNumberToCheck = 5;
            int flip = 0;
            if (numberWeAreChecking % 2 == 0 || numberWeAreChecking % 3 == 0)
            {
                return false;
            }
            else
            {
                var lastNumberToCheck = Math.Sqrt(numberWeAreChecking);
                for (var currentDivisor = firstNumberToCheck; currentDivisor <= lastNumberToCheck; currentDivisor += 2)
                {
                    if (numberWeAreChecking % currentDivisor == 0)
                    {
                        return false;
                    }
                    if (flip == 1)
                    {
                        currentDivisor += 2;
                        flip -= 1;
                    }
                    else { 
                        flip += 1; }
                }
            }
            return true;
        }
    }
}
